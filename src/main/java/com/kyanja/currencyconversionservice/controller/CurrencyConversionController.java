package com.kyanja.currencyconversionservice.controller;

import com.kyanja.currencyconversionservice.dto.CurrencyConversionBean;
import com.kyanja.currencyconversionservice.proxy.CurrencyExchangeServiceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/v1/currency-conversion-service-api")
public class CurrencyConversionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyConversionController.class);

    private final CurrencyExchangeServiceProxy currencyExchangeServiceProxy;

    public CurrencyConversionController(CurrencyExchangeServiceProxy currencyExchangeServiceProxy) {
        this.currencyExchangeServiceProxy = currencyExchangeServiceProxy;
    }

    @GetMapping("/currency-converter/from/{from}/to/{to}/ quantity/{quantity}")
    //where {from} and {to} represents the column
//return a bean back
    public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity) {

        LOGGER.info("*****************************************************************");
        LOGGER.info(" convertCurrency invoked from currency : " + from);
        LOGGER.info(" convertCurrency invoked to currency : " + to);
        LOGGER.info(" convertCurrency invoked quantity : " + quantity);
        LOGGER.info("*****************************************************************");

        String exchangeCurrencyUrl = "http://localhost:8082/api/v1/currency-exchange-service-api/currency-exchange/from/{from}/to/{to}";
        LOGGER.info(" exchangeCurrencyUrl called : " + exchangeCurrencyUrl);
        //setting variables to currency exchange service
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("from", from);
        uriVariables.put("to", to);
        //calling the currency-exchange-service
        ResponseEntity<CurrencyConversionBean> responseEntity = new RestTemplate().getForEntity(exchangeCurrencyUrl, CurrencyConversionBean.class, uriVariables);
        LOGGER.info("*****************************************************************");
        LOGGER.info(" exchangeCurrencyUrl called : " + exchangeCurrencyUrl);
        LOGGER.info("*****************************************************************");
        LOGGER.info("*****************************************************************");
        LOGGER.info(" exchangeCurrencyParameters uriVariables called : " + uriVariables);
        LOGGER.info("*****************************************************************");
        CurrencyConversionBean response = responseEntity.getBody();
        LOGGER.info("*****************************************************************");
        LOGGER.info(" CurrencyConversionBean response returned : " + response);
        LOGGER.info("*****************************************************************");
        //creating a new response bean and getting the response back and taking it into Bean
        return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(), quantity, quantity.multiply(response.getConversionMultiple()), response.getPort());
    }


    @GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrencyFeign(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal quantity) {
        LOGGER.info("*****************************************************************");
        LOGGER.info(" currency-converter-feign invoked from currency : " + from);
        LOGGER.info(" currency-converter-feign invoked to currency : " + to);
        LOGGER.info(" currency-converter-feign invoked quantity : " + quantity);
        LOGGER.info("*****************************************************************");
        CurrencyConversionBean currencyConversionBean = currencyExchangeServiceProxy.getCurrencyConversion(from, to);
        LOGGER.info("*****************************************************************");
        LOGGER.info(" currencyExchangeServiceProxy invoked : " + from);
        LOGGER.info(" currencyExchangeServiceProxy invoked to currency : " + to);
        LOGGER.info(" currencyExchangeServiceProxy invoked quantity : " + quantity);
        LOGGER.info("*****************************************************************");
        LOGGER.info(" CurrencyConversionBean response returned : " + new CurrencyConversionBean(
                currencyConversionBean.getId(),
                from,
                to,
                currencyConversionBean.getConversionMultiple(),
                quantity,
                quantity.multiply(currencyConversionBean.getConversionMultiple()),
                currencyConversionBean.getPort()));
        LOGGER.info("*****************************************************************");
        return new CurrencyConversionBean(
                currencyConversionBean.getId(),
                from,
                to,
                currencyConversionBean.getConversionMultiple(),
                quantity,
                quantity.multiply(currencyConversionBean.getConversionMultiple()),
                currencyConversionBean.getPort());
    }
}

